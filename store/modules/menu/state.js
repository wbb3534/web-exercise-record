const state = () => ({
    globalMenu: [
        {
            parentName: '회원관리',
            menuLabel: [
                { id: 'MEMBER_ADD', icon: 'el-icon-tickets', currentName: '회원등록', link: '/', isShow: false },
                { id: 'MEMBER_EDIT', icon: 'el-icon-tickets', currentName: '회원수정', link: '/', isShow: false },
                { id: 'MEMBER_LIST', icon: 'el-icon-tickets', currentName: '회원리스트', link: '/', isShow: true },
                { id: 'MEMBER_DETAIL', icon: 'el-icon-tickets', currentName: '회원상세정보', link: '/', isShow: false }
            ]
        },
        {
            parentName: '운동기록 관리',
            menuLabel: [
                { id: 'EXERCISE_RECORD_LIST', icon: 'el-icon-tickets', currentName: '운동기록 리스트', link: '/exercise-record/list', isShow: true },
            ]
        },
    ],
    selectedMenu: 'MEMBER_LIST'
})

export default state
