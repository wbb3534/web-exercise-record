export default {
    DO_LIST: 'member/doList',
    DO_CREATE: 'member/doCreate',
    DO_UPDATE: 'member/doUpdate',
    DO_DETAIL: 'member/doDetail'
}
