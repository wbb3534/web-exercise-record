const BASE_URL = '/v1/member'

export default {
    DO_LIST: `${BASE_URL}/list`, //get
    DO_CREATE: `${BASE_URL}/data`, //post
    DO_UPDATE: `${BASE_URL}/member-info/{id}`, //put
    DO_DETAIL: `${BASE_URL}/data/{id}` //get
}
