const BASE_URL = '/v1/exercise-record'

export default {
    DO_LIST: `${BASE_URL}/list`, //get
    DO_CREATE: `${BASE_URL}/data/{memberId}`, //post
    DO_UPDATE: `${BASE_URL}/member-info/{id}`, //put
}
