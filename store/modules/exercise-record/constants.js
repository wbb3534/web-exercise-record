export default {
    DO_LIST: 'exercise-record/doList',
    DO_CREATE: 'exercise-record/doCreate',
    DO_UPDATE: 'exercise-record/doUpdate'
}
