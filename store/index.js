import customLoading from './modules/custom-loading'
import authenticated from './modules/authenticated'
import menu from './modules/menu'
import testData from './modules/test-data'
import member from './modules/member'
import exerciseRecord from './modules/exercise-record';

export const state = () => ({})

export const mutations = {}

export const modules = {
    customLoading,
    authenticated,
    menu,
    testData,
    member,
    exerciseRecord
}
