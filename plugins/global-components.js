import Vue from 'vue'

import LoadingIcon from '~/components/common/loading-icon'
Vue.component('LoadingIcon', LoadingIcon)

import MainTitle from '~/components/common/main-title.vue'
Vue.component('MainTitle', MainTitle)

import SubTitle from '~/components/common/sub-title.vue'
Vue.component('SubTitle', SubTitle)
